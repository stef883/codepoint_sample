Codepoint is a website that help its users 'learn by solving code challenges and completing courses'.
The admins can manage challenges, manage courses, manage users.
The users (non-admin) can submit challenges, that after review of the admin will be activated.
Users can also search challenges and courses by 'tags' or by 'name'.
Last but not least, the website support latex for mathematic challenges.

This website has been made using sinatra and mongodb.