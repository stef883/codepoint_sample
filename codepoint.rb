require 'set'
require 'sinatra'
require 'sinatra/flash'
require 'mongo'
require './models'
require './helpers'

configure do
	enable :sessions
	set :session_secret, 'hello_secret'
end

get '/' do
	recent_challenges = Array.new
	popular_courses = Array.new
	erb :index, :layout => :main_layout,
							:locals => {recent_challenges: recent_challenges,
													popular_courses: popular_courses}
end	

get '/profile' do
	authenticate
	user = User.where(email: session[:user]).first
	courses_complete = Array.new
	courses_incomplete = Array.new
	Course.each do |course|
		if is_registered?(user, course)
			if has_completed?(user, course)
				courses_complete.push course
			else
				courses_incomplete.push course
			end
		end
	end
	erb :profile, :layout => :main_layout,
								:locals => {user: user,
													  courses_complete: courses_complete,
														courses_incomplete: courses_incomplete}
end

get '/signup' do
	erb :signup, :layout => :main_layout 
end

get '/login' do
	if is_logged_in_user?
		flash[:already_logged_in] = "You are already logged in. Redirected to 'profile' page"
		redirect '/profile/'
	else
		erb :login, :layout => :main_layout 
	end
end

get '/logout' do
	if is_logged_in_user?
		session.delete(:user)
		flash[:success_logout] = "You successfully logged out"
		redirect '/login'
	else
		flash[:not_logged_in] = "You are not logged in. Redirected to login page"
		redirect '/login'
	end
end

get '/admin' do
	authenticate_admin
	erb :adminpanel, :layout => :main_layout
end

get '/challenges' do
	challenges = Challenge.all
	params.each do |key, value|
		new_value = Rack::Utils.escape_html(params[key])
		if not value == new_value
			flash[:found_html_in_user_input] = "No html tags are allowed as input"
			redirect '/signup'
		end
		params[key] = new_value
	end
	if params[:query] and params[:'search-by']
		# sanitize first
		if params[:'search-by'] == 'tag'
			query_keywords = params[:query].split(',')
			query_keywords.each do |keyword|
				keyword.strip!
			end
			query_keywords.each do |keyword|
				challenges = challenges.where(tags: keyword).where(status: 'active')
			end
		elsif params[:'search-by'] == 'name'
			query = params[:query]
			challenges = challenges.where(name: /#{query}/i).where(status: 'active')
		end		
	else
		challenges = challenges.where(status: 'active')
	end
	erb :challenges, :layout => :main_layout, 
									 :locals => {challenges: challenges}
end

get '/challenge/:challengeid' do
	challengeid = params[:challengeid]
	challenge = Challenge.where(challengeid: challengeid)
	if challenge.exists? and challenge.first[:status] == 'active'
		erb :challenge, :layout => :main_layout, 
										:locals => {challenge: challenge.first}
	elsif challenge.exists? and challenge.first[:status] == 'inactive'
		authenticate_admin
		erb :activate_challenge, :layout => :main_layout, 
										:locals => {challenge: challenge.first}
	else
		flash[:challenge_not_valid_id] = 'Not a challenge with this id'
		redirect '/challenges'
	end
end

get '/course/:courseid' do
	courseid = params[:courseid]
	course = Course.where(courseid: courseid)
	if course.exists?
		users_completed = Array.new
		User.each do |user|
			completed = true
			course.first.challenges.each do |challenge|
				if not challenge.users.to_a.include? user
					completed = false
				end
			end
			if completed
				users_completed.push user
			end
		end
		erb :course, :layout => :main_layout, 
								 :locals => {course: course.first,
								 						 users_completed: users_completed}
	else
		flash[:course_not_valid_id] = 'Not a course with this id'
		redirect '/courses'
	end
end

get '/courses' do
	courses = Course.all
	params.each do |key, value|
		new_value = Rack::Utils.escape_html(params[key])
		if not value == new_value
			flash[:found_html_in_user_input] = "No html tags are allowed as input"
			redirect '/signup'
		end
		params[key] = new_value
	end
	if params[:query] and params[:'search-by']
		if params[:'search-by'] == 'tag'
			query_keywords = params[:query].split(',')
			query_keywords.each do |keyword|
				keyword.strip!
			end
			query_keywords.each do |keyword|
				courses = courses.where(tags: keyword).where(status: 'active')
			end
		elsif params[:'search-by'] == 'name'
			query = params[:query]
			courses = courses.where(name: /#{query}/i).where(status: 'active')
		end		
	else
		courses = courses.where(status: 'active')
	end
	erb :courses, :layout => :main_layout, 
								:locals => {courses: courses}
end


get '/topranked' do
	users_ranked = Array.new
	User.each do |u|
		users_ranked.push u
	end
	users_ranked.sort_by! do |user|
		if user[:points].nil?
			user[:points] = 0
		end
	end
	erb :topranked, :layout => :main_layout,
									:locals => {users: users_ranked}
end

get '/addchallenge' do
	authenticate_admin
	default_challengeid = get_next_challengeid # get first available from mongo
	erb :addchallenge, :layout => :main_layout,
										 :locals => {:default_challengeid => default_challengeid}
end

get '/submitchallenge' do
	erb :submitchallenge, :layout => :main_layout
end

get '/deleteuser' do
	authenticate_admin
	users = User.all
	erb :deleteuser, :layout => :main_layout,
									 :locals => {:users => users}
end

get '/managecourses' do
	authenticate_admin
	courses = Course.all
	erb :managecourses, :layout => :main_layout,
									    :locals => {:courses => courses}
end

get '/addcourse' do
	authenticate_admin
	default_courseid = get_next_courseid
	erb :addcourse, :layout => :main_layout,
									:locals => {:default_courseid => default_courseid}
end

get '/help' do
	erb :help, :layout => :main_layout
end

get '/forgotpassword' do
	erb :forgotpassword, :layout => :main_layout
end

get '/inactivechallenges' do
	authenticate_admin
	challenges = Challenge.where(status: 'inactive')
	erb :inactivechallenges, :layout => :main_layout,
									:locals => {:challenges => challenges}
end

get '/manageusers' do
	authenticate_admin
	users_active = User.where(is_active: true)
	users_inactive = User.where(is_active: false)
	erb :manageusers, :layout => :main_layout,
										:locals => {:users_active => users_active,
															:users_inactive => users_inactive}
end

get '/activateuser/:userid' do
	authenticate_admin
	user = User.where(userid: params[:userid])
	if user.exists?
		user.first.update!(is_active: true)
		if user.first.save
			flash[:user_activated] = "User activated"
			redirect '/manageusers'
		else 
			'something happened'
		end
	else
		'something happened'
	end
end

get '/deactivateuser/:userid' do
	authenticate_admin
	user = User.where(userid: params[:userid])
	if user.exists?
		user.first.update!(is_active: false)
		if user.first.save
			flash[:user_deactivated] = "User deactivated"
			redirect '/manageusers'
		else
			'something happened'
		end
	else
		'something happened'
	end
end	

get '/deleteuser/:userid' do
	authenticate_admin
	User.delete_all({userid: params[:userid]})
	flash[:user_deleted] = 'User deleted'
	redirect '/manageusers'
end

post '/signup' do
	params.each do |key, value|
		new_value = Rack::Utils.escape_html(params[key])
		if not value == new_value
			flash[:found_html_in_user_input] = "No html tags are allowed as input"
			redirect '/signup'
		end
		params[key] = new_value
	end
	user = User.new(userid: get_next_userid, email: params[:email], password: params[:psw], is_admin: false, points: 0)
	if user.save
		redirect '/login'
	else
		flash[:email_in_use] = "This email is in use"
		redirect '/signup'
	end
end

post '/login' do
	params.each do |key, value|
		new_value = Rack::Utils.escape_html(params[key])
		if not value == new_value
			flash[:found_html_in_user_input] = "No html tags are allowed as input"
			redirect '/login'
		end
		params[key] = new_value
	end
	user = User.where(email: params[:email], password: params[:psw])
	if user.exists? and user.first[:is_active]
		session[:user] = user.first[:email]
		flash[:success_login] = "You successfully logged in!"
		if user.first[:is_admin]
			session[:is_admin] = true
			redirect '/admin'
		else
			session[:is_admin] = false
			redirect '/profile'
		end
	else
		flash[:fail_login] = "Wrong credentials"
		redirect '/login'
	end
end

post '/challenge/:challengeid' do
	params.each do |key, value|
		new_value = Rack::Utils.escape_html(params[key])
		if not value == new_value
			flash[:found_html_in_user_input] = "No html tags are allowed as input"
			redirect '/login'
		end
		params[key] = new_value
	end
	challenge = Challenge.where(challengeid: params[:challengeid])
	if challenge.exists?
		if challenge.first[:answer] == params[:answer].strip
			if is_logged_in_user?
				u = User.where(email: session[:user]).first
				# if challenge not already solved by the user
				if not u.challenges.include? challenge.first
					# add challenge to completed for the user
					u.challenges.push(challenge.first)
					points = challenge.first[:points]
					u.update(points: u[:points]+points)
					flash[:got_a_point] = "Correct. You got " + points.to_s + " points."
					redirect '/challenge/' + challenge.first[:challengeid].to_s
				else
					flash[:already_completed_challenge] = "Correct. Already completed challenge though. You got no points"
					redirect '/challenge/' + challenge.first[:challengeid].to_s
				end
			else
				flash[:correct_not_logged_in] = "Correct. You are not logged in though so you got no points"
				redirect '/challenge/' + challenge.first[:challengeid].to_s				
			end
		else
			flash[:wrong_answer] = "This is not the correct answer. Try again."
			redirect '/challenge/' + challenge.first[:challengeid].to_s
		end
	else
		flash[:challenge_not_valid_id] = 'Not a challenge with this id.'
	end
end

post '/course/:courseid' do
	course = Course.where(courseid: params[:courseid])
	if course.exists?
		if is_logged_in_user?
			#if not already registered this course
			user = User.where(email: session[:user]).first
			if not course.first.users.include? user
				# do both
				course.first.users.push user
				user.courses.push course

				redirect '/profile'
			else
				flash[:already_registered_course] = 'You have already registered this course.'
				redirect '/course/' + params[:courseid].to_s
			end	
		else
			flash[:only_for_logged_in_users] = 'Only for logged in users.'
			redirect '/login'
		end
	else
		flash[:course_not_valid_id] = 'Not a course with this id.'
		redirect '/challenges'
	end
end

post '/addchallenge' do
	authenticate_admin
	params.each do |key, value|
		new_value = Rack::Utils.escape_html(params[key])
		params[key] = new_value
	end
	puts params[:tags].split(',')
	doc = {
		challengeid: params[:challengeid].to_i,
		name: params[:name],
		tags: params[:tags].split(','),
		description: params[:description],
		example: params[:example],
		example_answer: params[:example_answer],
		answer: params[:answer].strip, 	
		status: 'active',
		readings: params[:readings].split(','),
	}
	# make
	challenge = Challenge.new(doc)
	if challenge.save
		"challenge inserted in db"
	else
		"something wrong"
	end
end

post '/deleteuser' do
	user = params[:users]
	User.where(email: user).delete
	flash[:user_deletion_success] = "User deleted successfully"
	redirect '/admin'
end
 
post '/addcourse' do
	params.each do |key, value|
		new_value = Rack::Utils.escape_html(params[key])
		params[key] = new_value
	end
	doc = {
		courseid: params[:courseid].to_i,
		name: params[:name],
		description: params[:description],
		tags: params[:tags],
		status: params[:status],
		readings: params[:readings].split(','),

	}
	course = Course.new(doc)
	challenges = params[:challenges].split(',')
	puts "challenges", challenges
	challenges.each do |challenge|
		course.challenges.push Challenge.where(challengeid: challenge)
	end
	if course.save
		"course inserted"
	else
		'not inserted'
	end
end

post '/submitchallenge' do
	params.each do |key, value|
		new_value = Rack::Utils.escape_html(params[key])
		params[key] = new_value
	end
	doc = {
		challengeid: get_next_challengeid,
		name: params[:name],
		description: params[:description],
		example: params[:example],
		example_answer: params[:example_answer],
		answer: params[:answer].strip,
		status: 'inactive',
		readings: params[:readings].split(',')
	}
		challenge = Challenge.new(doc)
	if challenge.save
		"challenge inserted in db"
	else
		"something wrong"
	end
end

post '/activatechallenge' do
	authenticate_admin
	challenge = Challenge.where(challengeid: params[:challengeid])
	if challenge.exists?
		challenge.first.update!(status: 'active')
		if challenge.first.save
			flash[:challenge_activated] = 'Challenge activated'
			redirect '/inactivechallenges'
		else
			'Something happened'
		end
	else
		flash[:challenge_not_valid_id] = 'Not a challenge with this id.'
		redirect '/profile'
	end
end