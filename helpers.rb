require 'logger'

def authenticate
	unless session[:user]
		redirect '/login'
	end
end

def authenticate_admin
	unless session[:user] == 'admin'
		flash[:admin_page] = "You should be admin to do this"
		redirect '/profile'
	end
end

def is_logged_in_user?
	session[:user] ? true : false
end

def get_next_challengeid
	challengeids = Set.new
	Challenge.each do |challenge|
		challengeids.add challenge[:challengeid]
	end	
	challengeid = 0
	free_challenge_id = false
	while not free_challenge_id
		challengeid += 1
		if not challengeids.include? challengeid
			free_challenge_id = true
		end
	end
	return challengeid
end

def get_next_courseid
	courseids = Set.new
	Course.each do |course|
		courseids.add course[:courseid]
	end	
	courseid = 0
	free_course_id = false
	while not free_course_id
		courseid += 1
		if not courseids.include? courseid
			free_course_id = true
		end
	end
	return courseid
end

def get_next_userid
	userids = Set.new
	User.each do |user|
		userids.add user[:userid]
	end	
	userid = 0
	free_user_id = false
	while not free_user_id
		userid += 1
		if not userids.include? userid
			free_user_id = true
		end
	end
	return userid
end

def has_completed?(user, course)
	# only if he has completed all challenges of course
	challenges = course.challenges.all
	completed = true
	challenges.each do |challenge|
		if not user.challenges.include? challenge
			completed = false
		end
	end
	completed
end

def is_registered?(user, course)
	is_registered = false
	user.courses.each do |c|
		if c == course
			is_registered = true
		end
	end
	is_registered
end

def create_admin_user
	user = User.new(userid: 1, email: 'admin', password: '1234', is_admin: true, is_active: true)
	if user.save
		'admin user created'
	else
		'admin user not created'
	end
end

def create_sample_data
	# create 10 challenges
	(1..10).each do |i|
		challenge = Challenge.new(
			challengeid: i,
			name: i.to_s,
			description: i.to_s,
			example: i.to_s,
			example_answer: i.to_s,
			answer: i.to_s
			)
		if challenge.save
			puts 'created a challenge'
		end
	end 
	# create 3 courses
	(1..3).each do |i|
		course = Course.new(
			courseid: i,
			name: i.to_s,
			description: i.to_s,
			)
		course.challenges.push Challenge.where({challengeid: 1}).first
		course.challenges.push Challenge.where({challengeid: 2}).first
		if course.save
			puts 'created a course'
		end
	end 
	return 
end

def insert_sample_challenges(json_file)
	require 'json' 

	json = File.read(json_file)
	challenges = JSON.parse(json, symbolize_names: true)
	challenges.each do |challenge_doc|
		challenge = Challenge.new challenge_doc
		if challenge.save
			puts ('Sample challenge with id ' + challenge_doc[:challengeid].to_s + ' inserted')
		else
			puts ('An error occured while trying to insert challenge with id ' + challenge_doc[:challengeid].to_s)
		end
	end
	return 
end

def insert_sample_courses(json_file)
	require 'json'

	json = File.read(json_file)
	courses = JSON.parse(json, symbolize_names: true)
	courses.each do |course_doc|
		remainder = course_doc.slice!(:challenges)
		puts course_doc
		course = Course.new remainder
		course_doc[:challenges].each do |challengeid|
			challenge = Challenge.where(challengeid: challengeid).first
			add_challenge_to_course(challenge, course) 
			puts "challenge inserted into course"
		end
		if course.save
			puts ('Sample course with id ' + remainder[:courseid].to_s + ' inserted')
		else
			puts ('An error occured while trying to insert course with id ' + remainder[:courseid].to_s)
		end
	end
	return 
end

def insert_sample_users(json_file)
	require 'json'

	json = File.read(json_file)
	users = JSON.parse(json, symbolize_names: true)
	users.each do |user_doc|
		user = User.new user_doc
		if user.save
			puts ('Sample user with email ' + user_doc[:email].to_s + ' inserted')
		else
			puts ('An error occured while trying to insert user with email ' + user_doc[:email] + '.')
		end
	end
	return 
end

def add_challenge_to_course(challenge, course)
	# if challenge not in course already
	course_challenges_ids = Array.new
	course.challenges.each do |chall|
		course_challenges_ids.push chall[:challengeid]
	end
	if not course_challenges_ids.include? challenge[:challengeid]
		course.challenges.push challenge
		puts "all good"
	else
			"challenge is already part of the course"
	end
end

def insert_sample_data
	create_admin_user
	insert_sample_users 'sample_users.json'
	insert_sample_challenges 'sample_challenges.json'
	insert_sample_courses 'sample_courses.json'  	 # then courses
	puts "Sample data inserted in db. This includes (admin user, challenges, courses)"
end