require 'mongoid'

# DB Setup
Mongoid.load!("mongoid.yml", :development)

class User
	include Mongoid::Document

	field :userid, type: Integer
	field :email, type: String
	field :password, type: String
	field :is_admin, type: Boolean, default: false # change to field role, type: String
	field :points, type:Integer, default: 0
	field :is_active, type:Boolean, default: false

	has_and_belongs_to_many :challenges
	has_and_belongs_to_many :courses

	validates :email, presence: true
	validates :password, presence: true
	validates :is_admin, presence: true
end

class Challenge
	include Mongoid::Document

	field :challengeid, type: Integer
	field :name, type: String
	field :description, type: String
	field :example, type: String
	field :example_answer, type: String
	field :answer, type: String
	field :tags, type: Array, default: []
	field :status, type: String, default: 'active'
	field :readings, type: Array, default: []
	field :points, type: Integer, default: 1

	has_and_belongs_to_many :users
	has_and_belongs_to_many :courses

	validates :challengeid, presence: true
	validates :description, presence: true
	validates :answer, presence: true
	validates :status, presence: true
	validates :challengeid, uniqueness: true
end

class Course
	include Mongoid::Document

	field :courseid, type: Integer
	field :name, type: String
	field :description, type: String, default: []
	field :tags, type: Array, default: []
	field :status, type: String, default: 'active'
	field :readings, type: Array, default: []
	
	has_and_belongs_to_many :challenges
	has_and_belongs_to_many :users #registered_userss

	validates :courseid, presence: true
	validates :name, presence: true
	validates :description, presence: true
	validates :courseid, uniqueness: true
end
